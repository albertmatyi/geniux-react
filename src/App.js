import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {

  constructor() {
    super();
    this.state = {
      posts:[]
    };
  }

  componentWillMount() {
    fetch('https://jsonplaceholder.typicode.com/posts')
    .then(response=>response.json())
    .then(posts=>this.setState(posts))
    .catch(e=>console.log(e));
  }
  render() {
    console.log(this.state.posts);
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>

        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>

        {this.state.posts.map( post => (
          <div class="post">
            <h2 class="title">{post.title}</h2>
            <p class="body">{post.body}</p>
          </div>
        ))}
        <p>end</p>
      </div>
    );
  }
}

export default App;
